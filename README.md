# Aplicación para entrenar contadores de migración de rapaces

La app consiste en una interfaz donde se despliega un video aleatorio con determinada cantidad de rapaces de las especies Aguililla alas anchas (BW), Aguililla de Swainson (SW) y Zopilote aura (TV); los usuarios pueden ingresar un identificador con sus estimaciones de cantidades por especie y la aplicación devuelve el error de estimación para cada especie, conservando un registro por usuario para medir su progreso.


![video_aves](aves_320.gif){ width=15% }


## Generador de videos

El archivo `entrena_observa_aves.R` contiene un código que a partir de cantidades manualmente ingresadas, genera un video con aves distribuídas aleatoriamente (normal en la horizontal, uniforme en la vertical); una serie de ajustes `ffmpeg` sirven para rotar el video o ponerle una transparencia que imita la resolana en nubes cirros.

Se revisaron otras posibles formas de generar los animaciones instantáneas, con sus consiguientes observaciones:

* canvas API de html/Javascript: la mejor opción para crear instantáneamente las visualizaciones, pero requiere más tiempo de desarrollo
* plotly.JS: facilidad para crear instantáneamente la animación, pero no permite usar marcadores de imágenes para mostrar las aves
* matplotlib.animate de python: no permite generar la animación instantáneamente

## interfaz shiny

La aplicación se montó como una ShinyApp, por su versatilidad para el flujo I/O de datos.

![ejemplo shiny](shinyapp.png){ width=15% }
